package com.devansh.forcastr;
/**
 * Created by Devansh Jain on 22/7/2017
 */

// Remove all Comments from below code for using activity_main

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.widget.TextView;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    TextView cityField, detailsField, currentTemperatureField, humidity_field, pressure_field, weatherIcon, updatedField, greeting, greeting2;

    Typeface weatherFont;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Use   setContentView(R.layout.activity_main);   for enabling all details
        setContentView(R.layout.custom_layout);


        weatherFont = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/weathericons-regular-webfont.ttf");

        greeting = (TextView) findViewById(R.id.greeting);
       /* cityField = (TextView) findViewById(R.id.city_field);
        updatedField = (TextView) findViewById(R.id.updated_field);
        detailsField = (TextView) findViewById(R.id.details_field);*/
        currentTemperatureField = (TextView) findViewById(R.id.current_temperature_field);
        /*humidity_field = (TextView) findViewById(R.id.humidity_field);
        pressure_field = (TextView) findViewById(R.id.pressure_field);*/
        weatherIcon = (TextView) findViewById(R.id.weather_icon);
        weatherIcon.setTypeface(weatherFont);
        greeting2 = (TextView) findViewById(R.id.textView2);


        Function.placeIdTask asyncTask = new Function.placeIdTask(new Function.AsyncResponse() {
            public void processFinish(String weather_city, String weather_description, String weather_temperature, String weather_humidity, String weather_pressure, String weather_updatedOn, String weather_iconText, String sun_rise) {

                //cityField.setText(weather_city);
                //updatedField.setText(weather_updatedOn);
                //detailsField.setText(weather_description);
                currentTemperatureField.setText(weather_temperature);
                //humidity_field.setText("Humidity: " + weather_humidity);
                //pressure_field.setText("Pressure: " + weather_pressure);
                Log.d(TAG, "processFinish: " + weather_iconText);
                if (weather_iconText == "&#xf01e;") {
                    greeting2.setText("");
                } else if (weather_iconText == "&#xf01c;") {
                    greeting2.setText("");
                } else if (weather_iconText == "&#xf014;") {
                    greeting2.setText("");
                } else if (weather_iconText == "&#xf013;") {
                    greeting2.setText("");
                } else if (weather_iconText == "&#xf01b;") {
                    greeting2.setText("");
                } else{
                    greeting2.setText(getResources().getString(R.string.be));
                }
                weatherIcon.setText(Html.fromHtml(weather_iconText));

            }
        });

        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        Criteria criteria = new Criteria();

        String provider = locationManager.getBestProvider(criteria, false);
        if(ActivityCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION)!=PackageManager.PERMISSION_GRANTED &&ActivityCompat.checkSelfPermission(this,Manifest.permission.ACCESS_COARSE_LOCATION)!=PackageManager.PERMISSION_GRANTED)
        {
            return;
        }

        Location location = locationManager.getLastKnownLocation(provider);
        double latitude = location.getLatitude();
        double longitude = location.getLongitude();

        asyncTask.execute(latitude+"",longitude+"");

        Calendar c = Calendar.getInstance();
        int timeOfDay = c.get(Calendar.HOUR_OF_DAY);

        if(timeOfDay >=0&&timeOfDay< 12)

        {
            greeting.setText("Good Morning!");
        }else if(timeOfDay >=12&&timeOfDay< 16)

        {
            greeting.setText("Good Afternoon!");
        }else if(timeOfDay >=16&&timeOfDay< 21)

        {
            greeting.setText("Good Evening!");
        }else if(timeOfDay >=21&&timeOfDay< 24)

        {
            greeting.setText("Good Night!");
        }

    }

}